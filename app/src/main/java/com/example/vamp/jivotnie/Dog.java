package com.example.vamp.jivotnie;

import android.util.Log;
import android.util.StringBuilderPrinter;

public class Dog extends Animals {
    private static final String LOG_TAB = "Dog";
    private String Name = "Том и Рекс";
    private int ImageIdDog = R.drawable.dog_cat; // Указывае где картинка

    public Dog (String rex){
        this.Name = Name;
    }
    @Override
    public void eat() {
        Log.d(LOG_TAB,"Кушает");
    }
    @Override
    public void sleep() {Log.d(LOG_TAB,"Спит");}
    @Override
    public int GetImageId() {return ImageIdDog;}
    @Override
    public String GetName() {return Name;}

}
