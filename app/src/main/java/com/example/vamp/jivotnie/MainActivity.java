package com.example.vamp.jivotnie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.jar.Attributes;

public class MainActivity extends AppCompatActivity {
    LinearLayout lin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //LinearLayout layout =(LinearLayout) findViewById(R.id.layout); // Сделать
        //layout = (LinearLayout) findViewById(R.id.layout);
        lin = (LinearLayout) findViewById(R.id.layout_edit);
        ArrayList<Animals> list = new ArrayList<>(); // Создаем лист
        list.add(new Cat("")); // Даю имена сразу в классе так не получилось через лист
        list.add(new Dog(""));
        list.add(new Mouse(""));
        list.add(new Tigr(""));
        list.add(new Volk(""));

        for(Animals  animal: list){   // Создаем цикл foreach
            TextView name = new TextView(this);
            ImageView image = new ImageView(this);
            image.setLayoutParams(new ViewGroup.LayoutParams(270,270));
            image.setImageResource(animal.GetImageId());
            lin.addView(image);
            name.setText(animal.GetName());
            lin.addView(name);
        }
    }
}
