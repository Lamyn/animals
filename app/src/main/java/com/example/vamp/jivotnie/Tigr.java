package com.example.vamp.jivotnie;

import android.util.Log;

public class Tigr extends Animals {
    private static final String LOG_TAG = "Tigr";
    private String Name = "Агри";
    private int ImageID = R.drawable.tigr;
    public Tigr(String tig){this.Name = Name;}
    @Override
    public void eat() {Log.d(LOG_TAG,"Кушает");}
    @Override
    public void sleep() {Log.d(LOG_TAG,"Спит");}
    @Override
    protected int GetImageId() {return ImageID;}
    @Override
    protected String GetName() {return Name;}
}
