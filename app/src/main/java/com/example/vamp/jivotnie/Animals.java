package com.example.vamp.jivotnie;
public abstract class Animals {
    public abstract void eat();
    public abstract void sleep();

    protected abstract int GetImageId();
    protected abstract String GetName();
}
