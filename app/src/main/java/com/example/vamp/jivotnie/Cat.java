package com.example.vamp.jivotnie;

import android.util.Log;

import java.util.jar.Attributes;

public class Cat extends Animals {
    private static final String LOG_TAB = "Cat";
    private String Name = "Берти";
    private int ImageIdCat = R.drawable.cat; // Указывае где картинка
    public Cat (String tom){
        this.Name = Name;
    }
    @Override
    public void eat() {
        Log.d(LOG_TAB,"кушает");
    }
    @Override
    public void sleep() {
        Log.d(LOG_TAB,"спит");
    }
    @Override
    public int GetImageId() {return ImageIdCat;}
    @Override
    public String GetName() {return Name;}
}
